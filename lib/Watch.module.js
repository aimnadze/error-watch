const fs = require('fs')

module.exports = app => directories => {

    function scan (directory) {
        watch(directory)
        fs.readdirSync(directory).forEach(item => {
            const filename = directory + '/' + item
            if (fs.statSync(filename).isDirectory()) scan(filename)
            else watch(filename)
        })
    }

    function watch (filename) {
        fs.watch(filename, () => {
            app.log.info('Source changed')
            process.exit()
        })
    }

    if (app.config.debug_mode) directories.forEach(scan)

}
