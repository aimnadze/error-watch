const fs = require('fs')

const nodemailer = require('nodemailer')

const config_smtp = require('../config').smtp

module.exports = options => {

    function next () {
        if (!queued) return
        queued = false
        fs.readFile(options.file, 'utf8', (err, text) => {
            ;(done => {

                if (err) {
                    log.error('Read error')
                    done()
                    return
                }

                if (text.length === 0) {
                    log.info('Truncated')
                    done()
                    return
                }

                log.info('Sending')

                const transport = nodemailer.createTransport({
                    host: config_smtp.host,
                    port: config_smtp.port,
                    auth: {
                        user: config_smtp.username,
                        pass: config_smtp.password,
                    },
                })
                transport.sendMail({
                    from: config_smtp.from,
                    to: config_smtp.to,
                    subject: options.file,
                    text,
                }, (err, info) => {

                    if (err) {
                        log.error('Sending failed', err.code)
                        done()
                        return
                    }

                    log.info('Sent')
                    done()
                })

            })(next)
        })
    }

    const log = options.log

    let queued = false

    return () => {
        log.info('Changed')
        queued = true
        next()
    }

}
