const fs = require('fs')

module.exports = app => () => {

    app.config.watch.forEach(file => {
        ;(log => {

            const send = app.Send({ file, log })

            let abort = () => {}
            fs.watch(file, () => {
                abort()
                abort = app.Timeout(send, 100)
            })

        })(app.log.sublog(file))
    })

    app.Watch(['lib'])

    app.log.info('Started')

}
