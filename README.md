Error Watch
===========

A program watches a set of files and sends an email when they change.

Motivation
----------

When you run an application in background
you usually redirect its `stdout` and `stderr` to files.
You can later see what the program has printed there.
If the application crashes the crash reason is printed in its `stderr`.
Error Watch can watch these files and send you
the crash reason as soon as it happens.

Scripts
-------

* `./restart.sh` - start/restart the program.
* `./stop.sh` - stop the program.
* `./clean.sh` - clean the program after an unexpected shutdown.
* `./rotate.sh` - clean old logs.

Configuration
-------------

`config.js` contains the configuration.

Technology
----------

* Node.js
* `nodemailer` NPM package
